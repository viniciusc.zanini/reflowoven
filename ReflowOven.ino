#include "Sensor.h"
#include "PID.h"
#include "ReflowProfile.h"
#include "Display.h"
#include "Hatch.h"

/* 
* ######################################################################
* ########################## Reflow Oven v1.0 ##########################
* ######################################################################
*
* To set the PID constants, go to PID.h (KP, KI, KD)
* To set the reflow profile, go to ReflowProfile.cpp (reflow_profile)
* 
* Written by: VCZ,
*/


#define GATE_PIN 6
#define LED_PIN 5

bool start_cycle = false;
int xv = 0;
int loop_counter = 0;
String title = "";
bool closed = false;

double temp = 0.0;

void setup() {
  Serial.begin(57600);

  pinMode(LED_PIN, OUTPUT);
  pinMode(GATE_PIN, OUTPUT);
  pinMode(A2, INPUT);

  sensorBegin();

  displayBegin();

  hatchBegin();


  // Set timer2 interrupt at 1Hz
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 8khz increments
  OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS21 bit for 8 prescaler
  TCCR2B |= (1 << CS21);   
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
}

void loop() {
  // Start reflow process if start_cycle = tru
  if(start_cycle){
    displayClear();

    for(double i = 0; i <= 360; i = i + 0.05) {
    
      // Get filtered temperature
      double temp =  sensorGetFilteredTemp();

      // Get set point
      double setpoint = getReflowProfileAtX(i, 0.05);

      // Get PID result
      double pid = PIDControl(temp, setpoint);

      // Set gate
      if(xv < 280){
        digitalWrite(GATE_PIN, HIGH);
        digitalWrite(LED_PIN, HIGH);
        delay(pid);
        digitalWrite(GATE_PIN, LOW);
        digitalWrite(LED_PIN, LOW);
        delay(50 - pid);
      }else {
        digitalWrite(GATE_PIN, LOW);
        digitalWrite(LED_PIN, LOW);
        delay(50);
      }


      if(loop_counter % 20 == 0) { // 1Hz
        // Serial.print(" Temp: ");
        // Serial.print(temp);
        // Serial.print(" Sp: ");
        // Serial.println(setpoint);

        title = "";
        title += "T:";
        title +=  String(temp, 0); 
        title +=  "C S:"; 
        title +=  String(setpoint, 0); 
        title +=  "C P:"; 
        title +=  String(xv);
        title +=   "s";

        Serial.println(title);
        Serial.print(setpoint);
        Serial.print(" ");
        Serial.println(temp);

        if(xv == 280){
          title = "Hatch opening...";
          displayGraph(xv, temp, title);
          hatchOpen();
        }
        
        displayGraph(xv, temp, title);

        xv++;
      }
      loop_counter++;

      if(!start_cycle) {
        break;
      }
    }
    xv = 0;
    loop_counter = 0;
    clearReflowProfile();
    displayClear();
    start_cycle = false;
    closed = false;
  }

  temp = sensorGetFilteredTemp();

  displayHome(temp, "Ready to start...");

  if(temp < 60 && !closed){
      displayHome(temp, "Hatch closing...");
       hatchClose();
       closed = true;
  }

  delay(100);
}

int countStop = 0;
ISR(TIMER2_COMPA_vect){
  if(!digitalRead(A2)){
    countStop++;
  }

  if(countStop >= 8000){
    countStop = 0;
    start_cycle = !start_cycle;
  }
}
