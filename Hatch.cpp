 #include "hatch.h"


void hatchBegin() {
  pinMode(SERVO_PIN, OUTPUT);
  digitalWrite(SERVO_PIN, LOW);
}

void hatchOpen(){
  for(int i = 0; i < TURNS; i ++){
    digitalWrite(SERVO_PIN, HIGH);
    delayMicroseconds(POS1);    //position
    digitalWrite(SERVO_PIN, LOW);
    delayMicroseconds(20000 - POS1);   //balance of 20000 cycle
  }
}

void hatchClose(){
  int pos = 2000;
  for(int i = 0; i < TURNS; i ++){
    digitalWrite(SERVO_PIN, HIGH);
    delayMicroseconds(pos);    //position
    digitalWrite(SERVO_PIN, LOW);
    delayMicroseconds(20000 - pos);   //balance of 20000 cycle
    pos -= 20;
  }
}