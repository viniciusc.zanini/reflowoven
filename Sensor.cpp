#include "Sensor.h"

// Variables
int sensor_temp_array[FILTER_SIZE];
int old_sensor_average = 0;
int rawRead = 0;


Thermistor* thermistor = new NTC_Thermistor(
    SENSOR_PIN,
    REFERENCE_RESISTANCE,
    NOMINAL_RESISTANCE,
    NOMINAL_TEMPERATURE,
    B_VALUE
  );

// Functions
void sensorBegin() {
  for (int i = 0; i < FILTER_SIZE; i++) {
    sensor_temp_array[i] = analogRead(SENSOR_PIN);
    delay(50);
  }
}

float convertToCelsius(int rawValue) {
  float readResistance = REFERENCE_RESISTANCE / (1023.0 / rawValue - 1.0);
  return thermistor->resistanceToKelvins(readResistance) - 273.5;
}

float sensorGetFilteredTemp() {
  int sensor_array_sum = 0.0;

  // Shift array to the left <<
  for (int i = 0; i < FILTER_SIZE - 1; i++) {
    sensor_temp_array[i] = sensor_temp_array[i + 1];
  }

  // Put the latest read on the last position
  rawRead = analogRead(SENSOR_PIN);

  // Apply the trust factor
  sensor_temp_array[FILTER_SIZE - 1] = ((rawRead * TRUST_FACTOR) + (old_sensor_average * (1 - TRUST_FACTOR)));
   
  // Sum all the array positions
  for (int i = 0; i < FILTER_SIZE; i++) {
    sensor_array_sum += sensor_temp_array[i];
  }

  old_sensor_average = sensor_array_sum / FILTER_SIZE;

 return convertToCelsius(old_sensor_average);
}
