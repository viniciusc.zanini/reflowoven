#ifndef PID_H_
#define PID_H_

#include <Arduino.h>

#define KK 1

// Define here the PID constants:
#define KP 2.2
#define KI 0.01
#define KD 7

extern double error;
extern double last_error;

extern double p_term;
extern double i_term;
extern double d_term;

extern double integrated_error;

extern double guard_gain;

int PIDControl(double value, double set_point);

#endif