#include "display.h"


// These are a required variables for the graphing functions
bool redraw = true;
 double ox = 0.0;
 double oy = 0.0;

// Create the display object
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);



void displayBegin() {
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.clearDisplay();
    display.display();

    displayLogo();

    delay(3000);

    display.clearDisplay();
    display.display();
}

void displayClear() {
    redraw = true;
    display.clearDisplay();
    //display.display();
}

void displayHome(double temp, String title){
    display.clearDisplay();
    // Header
    display.fillRect(0, 0,  127 , 16, SSD1306_WHITE);
    display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    display.setTextSize(1);
    display.setCursor(2, 4);
    display.println(title);

    // Body
    //display.fillRect(0, 16,  127 , 48, SSD1306_WHITE);
    display.setTextColor( SSD1306_WHITE, SSD1306_BLACK);
    display.setTextSize(3);
    display.setCursor(0, 30);
    display.print(temp);
    display.println("C");


    display.display();

}

void displayLogo() {
    display.clearDisplay();
    //                 x   y
    display.drawBitmap(32, 0, logo_bmp, LOGO_WIDTH, LOGO_HEIGHT, SSD1306_WHITE);
    display.display();
}


void displayGraph(double xv, double yv, String title) {
    // Header
    display.fillRect(0, 0,  127 , 16, SSD1306_WHITE);
    display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    display.setTextSize(1);
    display.setCursor(2, 4);
    display.println(title);

    // Body
    DrawCGraph(display, xv, yv, 1, 63, 126, 46, 0, 360, 25, 0, 280, 130, 0, title, redraw);
}

void DrawCGraph(Adafruit_SSD1306 &d, double x, double y, double gx, double gy, double w, double h, double xlo, double xhi, double xinc, double ylo, double yhi, double yinc, double dig, String title, boolean &Redraw) {
    double i;
    double temp;
    int rot, newrot;

    // d.fillRect(0, 0,  127 , 16, SSD1306_WHITE);
    // d.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    // d.setTextSize(1);
    // d.setCursor(2, 4);
    // d.println(title);
        
    if (Redraw == true) {
        Redraw = false;
        
        ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
        oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
        // draw y scale
        d.setTextSize(1);
        d.setTextColor(SSD1306_WHITE, SSD1306_BLACK);
        for ( i = ylo; i <= yhi; i += yinc) {
            // compute the transform
            // note my transform funcition is the same as the map function, except the map uses long and we need doubles
            temp =  (i - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
            if (i == 0) {
                d.drawFastHLine(gx - 3, temp, w + 3, SSD1306_WHITE);
            }
            else {
                d.drawFastHLine(gx - 3, temp, 3, SSD1306_WHITE);
            }
            d.setCursor(gx - 27, temp - 3);
            d.println(i, dig);
        }
        // draw x scale
        for (i = xlo; i <= xhi; i += xinc) {
            // compute the transform
            d.setTextSize(1);
            d.setTextColor(SSD1306_WHITE, SSD1306_BLACK);
            temp =  (i - xlo) * ( w) / (xhi - xlo) + gx;
            if (i == 0) {
                d.drawFastVLine(temp, gy - h, h + 3, SSD1306_WHITE);
            }
            else {
                d.drawFastVLine(temp, gy, 3, SSD1306_WHITE);
            }
            d.setCursor(temp, gy + 6);
            d.println(i, dig);
        }
    }

    // graph drawn now plot the data
    // the entire plotting code are these few lines...

    x =  (x - xlo) * ( w) / (xhi - xlo) + gx;
    y =  (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
    d.drawLine(ox, oy, x, y, SSD1306_WHITE);
    d.drawLine(ox, oy - 1, x, y - 1, SSD1306_WHITE);
    ox = x;
    oy = y;

    // up until now print sends data to a video buffer NOT the screen
    // this call sends the data to the screen
    d.display();
}
