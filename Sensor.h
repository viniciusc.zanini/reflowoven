#ifndef SENSOR_H_
#define SENSOR_H_

#include <Arduino.h>
#include "Sensor.h"
#include <Thermistor.h>
#include <NTC_Thermistor.h>

// Definitions
#define SENSOR_PIN             A3
#define REFERENCE_RESISTANCE   100000
#define NOMINAL_RESISTANCE     100000
#define NOMINAL_TEMPERATURE    25
#define B_VALUE                3950

#define TRUST_FACTOR 0.95
#define FILTER_SIZE 10

// Variables
extern int sensor_temp_array[FILTER_SIZE];

extern Thermistor* thermistor;

// Functions
void sensorBegin();

float convertToCelsius(int rawValue) ;

float sensorGetFilteredTemp();


#endif
