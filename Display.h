#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "Logo.h"

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET          A4

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define SCREEN_ADDRESS 0x3C

// These are a required variables for the graphing functions
extern bool redraw;
extern double ox;
extern double oy;



void displayBegin();

void displayClear();

void displayLogo();

void displayHome(double temp, String title);

void displayGraph(double xv, double yv, String title);

void DrawCGraph(Adafruit_SSD1306 &d, double x, double y, double gx, double gy, double w, double h, double xlo, double xhi, double xinc, double ylo, double yhi, double yinc, double dig, String title, boolean &Redraw);


#endif
