#include "PID.h"

double error = 0.0;
double last_error = 0.0;

double p_term = 0.0;
double i_term = 0.0;
double d_term = 0.0;

double integrated_error = 0.0;

double guard_gain = 1000.0;


int PIDControl(double value, double set_point) {
  error = set_point - value;

  p_term = KP * error;

  integrated_error += error;
  i_term = KI * constrain(integrated_error, -guard_gain, guard_gain);

  d_term = KD * (error - last_error);

  last_error = error;
  
 // Serial.println((p_term + i_term + d_term));
  return constrain(KK * (p_term + i_term + d_term), 0, 50);
}