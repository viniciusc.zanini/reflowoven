#ifndef REFLOW_PROFILE_H_
#define REFLOW_PROFILE_H_

// Variables
extern double reflow_profile [5][2];

extern double y;

// Functions
void clearReflowProfile();
double getReflowProfileAtX(double x, double freq);


#endif
