#ifndef HATCH_H_
#define HATCH_H_

#include <Arduino.h>

// Definitions
#define TURNS 200
#define POS1 2000
#define POS2 1000 
#define SERVO_PIN 3

// Functions
void hatchBegin();
void hatchOpen();
void hatchClose();

#endif