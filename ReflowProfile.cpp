#include "ReflowProfile.h"

// Variables

// Define here the reflow profile:
double reflow_profile [5][2] = {
  //sec  °C
  {90,  150}, // Preheat
  {180, 180}, // Soak
  {210, 250}, // Reflow
  {280, 250}, // Dwell
  {360, 0  }  // Cool down
};

double y = 0.0;

// Functions

void clearReflowProfile(){
  y = 0.0;
}

double getReflowProfileAtX(double x, double freq) { // 4Hz
  if (x < reflow_profile[0][0]) { // Preheat
    y += ((reflow_profile[0][1]) / reflow_profile[0][0]) * freq;
  } else if (x > reflow_profile[0][0] && x <= reflow_profile[1][0]) { // Soak
    y += ((reflow_profile[1][1] - reflow_profile[0][1]) / (reflow_profile[1][0] - reflow_profile[0][0])) * freq;
  } else if (x > reflow_profile[1][0] && x <= reflow_profile[2][0]) { // Reflow
    y += ((reflow_profile[2][1] - reflow_profile[1][1]) / (reflow_profile[2][0] - reflow_profile[1][0])) * freq;
  } else if (x > reflow_profile[2][0] && x <= reflow_profile[3][0]) { // Dwell
    y += ((reflow_profile[3][1] - reflow_profile[2][1]) / (reflow_profile[3][0] - reflow_profile[2][0])) * freq;
  } else if (x > reflow_profile[3][0] && x <= reflow_profile[4][0]) { // Cool down
    y += ((reflow_profile[4][1] - reflow_profile[3][1]) / (reflow_profile[4][0] - reflow_profile[3][0])) * freq;
  }

  return y;
}